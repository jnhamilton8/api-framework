import random
import string

import requests
from assertpy import assert_that


class PeopleUtils(object):

    @staticmethod
    def create_new_person(first_name, last_name):
        payload = {
            "fname": first_name,
            "lname": last_name
        }
        return payload

    @staticmethod
    def post_new_person_and_return_person_id(people, first_name, last_name):
        payload = PeopleUtils.create_new_person(first_name, last_name)
        post_response = people.post_new_person(payload)
        assert_that(post_response.status_code, description='Person not created').is_equal_to(requests.codes.no_content)

        get_response = people.get_all_people()
        assert_that(get_response.status_code, description='Get failed to return response').is_equal_to(
            requests.codes.all_ok)

        get_response_data = get_response.json()

        # iterate over list of dicts until find a match and return as dict, else return None
        new_person_record = next(
            (item for item in get_response_data if item['fname'] == first_name
             and item['lname'] == last_name), None)

        return new_person_record['person_id']

    @staticmethod
    def random_string_generator(str_size):
        return ''.join(random.choice(string.ascii_letters) for x in range(str_size))

    @staticmethod
    def return_list_of_first_and_last_names(number_of_people):
        first_and_last_names = list()

        for i in range(number_of_people):
            first_name = PeopleUtils.random_string_generator(5)
            last_name = PeopleUtils.random_string_generator(8)
            first_and_last_names.append({'fname': first_name, 'lname': last_name})

        return first_and_last_names

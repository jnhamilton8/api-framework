import pytest
import requests
from assertpy import assert_that

from core.people import People
from tests.people_utils import PeopleUtils


@pytest.fixture(autouse=True)
def people():
    people = People()
    people.first_name = PeopleUtils.random_string_generator(5)
    people.last_name = PeopleUtils.random_string_generator(8)
    yield people

    # teardown
    people.delete(people.person_id)


def test_new_person_can_be_created(people):
    payload = PeopleUtils.create_new_person(people.first_name, people.last_name)

    post_response = people.post_new_person(payload)
    assert_that(post_response.status_code, description='Person not created').is_equal_to(requests.codes.no_content)

    get_response = people.get_all_people()
    get_response_data = get_response.json()

    assert_that(get_response.status_code, description='Get failed to return response').is_equal_to(
        requests.codes.all_ok)
    assert_that(get_response_data).extracting('fname').contains(people.first_name)
    assert_that(get_response_data).extracting('lname').contains(people.last_name)

    # iterate over list of dicts until find a match and return as dict, else return None
    new_person_record = next(
        (item for item in get_response_data if item['fname'] == people.first_name
         and item['lname'] == people.last_name), None)

    # set for cleanup
    people.person_id = new_person_record['person_id']


def test_post_new_person_with_empty_body_returns_204_and_creates_record_with_null_data(people):
    payload = {}
    post_response = people.post_new_person(payload)

    assert_that(post_response.status_code).is_equal_to(requests.codes.no_content)
    assert_that(post_response.status_code, description='Person not created').is_equal_to(requests.codes.no_content)

    get_response = people.get_all_people()
    get_response_data = get_response.json()

    assert_that(get_response_data).extracting('fname').contains(None)
    assert_that(get_response_data).extracting('lname').contains(None)

    # iterate over list of dicts until find a match and return as dict, else return None
    new_person_record = next(
        (item for item in get_response_data if item['fname'] is None
         and item['lname'] is None), None)

    # set for cleanup
    people.person_id = new_person_record['person_id']

import pytest
import requests
from assertpy import assert_that

from core.people import People
from tests.people_utils import PeopleUtils


@pytest.fixture(autouse=True)
def people():
    people = People()
    people.first_name = PeopleUtils.random_string_generator(5)
    people.last_name = PeopleUtils.random_string_generator(8)
    people.person_id = PeopleUtils.post_new_person_and_return_person_id(people, people.first_name,
                                                                        people.last_name)
    yield people


def test_person_can_be_deleted_by_id(people):
    delete_response = people.delete(people.person_id)
    assert_that(delete_response.status_code).is_equal_to(requests.codes.all_ok)
    assert_that(delete_response.text).is_equal_to(f'Person with id {people.person_id} successfully deleted')

    get_response = people.get_all_people()
    assert_that(get_response.status_code, description='Get failed to return response').is_equal_to(
        requests.codes.all_ok)

    get_response_data = get_response.json()
    assert_that(get_response_data).does_not_contain({'fname': people.first_name, 'lname': people.last_name})


def test_deleting_person_with_invalid_id_returns_404(people):
    people.person_id = 20000000000000
    delete_response = people.delete(people.person_id)
    delete_response_data = delete_response.json()

    assert_that(delete_response.status_code).is_equal_to(requests.codes.not_found)
    assert_that(delete_response_data['title']).is_equal_to('Not Found')
    assert_that(delete_response_data['detail']).is_equal_to(f'Person not found for id {people.person_id}')


def test_deleting_person_omitting_id_returns_404(people):
    people.person_id = None
    delete_response = people.delete(people.person_id)
    delete_response_data = delete_response.json()

    assert_that(delete_response.status_code).is_equal_to(requests.codes.not_found)
    assert_that(delete_response_data['title']).is_equal_to('Not Found')
    assert_that(delete_response_data['detail']).is_equal_to('The requested URL was not found on the server. '
                                                            'If you entered the URL manually please check your '
                                                            'spelling and try again.')

import json

import pytest
from assertpy import assert_that, soft_assertions
from cerberus import Validator

from core.people import People
from tests.people_utils import PeopleUtils

schema = {
    "fname": {'type': 'string'},
    "lname": {'type': 'string'},
    "person_id": {'type': 'number'},
    "timestamp": {'type': 'string'}
}


@pytest.fixture(autouse=True)
def people():
    people = People()
    people.first_name = PeopleUtils.random_string_generator(5)
    people.last_name = PeopleUtils.random_string_generator(8)
    people.person_id = PeopleUtils.post_new_person_and_return_person_id(people, people.first_name,
                                                                        people.last_name)
    yield people

    # teardown
    people.delete(people.person_id)


def test_read_all_operation_has_expected_schema(people):
    response = people.get_all_people()
    persons = json.loads(response.text)

    validator = Validator(schema, require_all=True)

    with soft_assertions():
        for person in persons:
            is_valid = validator.validate(person)
            assert_that(is_valid, description=validator.errors).is_true()

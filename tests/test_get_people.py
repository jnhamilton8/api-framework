import pytest
import requests
from assertpy import assert_that

from core.people import People
from tests.people_utils import PeopleUtils


@pytest.fixture(autouse=True)
def people():
    people = People()
    yield people

    # teardown
    for person in people.person_ids:
        people.delete(person)


def test_can_get_person_by_id(people):
    people.first_name = PeopleUtils.random_string_generator(5)
    people.last_name = PeopleUtils.random_string_generator(8)
    people.person_id = PeopleUtils.post_new_person_and_return_person_id(people, people.first_name,
                                                                        people.last_name)
    get_response = people.get_person_by_id(people.person_id)
    get_response_data = get_response.json()

    assert_that(get_response.status_code, description='Get failed to return response').is_equal_to(
        requests.codes.all_ok)
    assert_that(get_response_data).contains_entry({'fname': people.first_name}, {'lname': people.last_name})
    assert_that(get_response_data).contains_key('person_id', 'timestamp')


def test_can_get_all_people(people):
    people_data = PeopleUtils.return_list_of_first_and_last_names(2)

    people.person_ids.append(PeopleUtils.post_new_person_and_return_person_id(people,
                                                                              people_data[0]['fname'],
                                                                              people_data[0]['lname']))
    people.person_ids.append(PeopleUtils.post_new_person_and_return_person_id(people,
                                                                              people_data[1]['fname'],
                                                                              people_data[1]['lname']))

    get_response = people.get_all_people()
    get_response_data = get_response.json()

    assert_that(get_response.status_code, description='Get failed to return response').is_equal_to(
        requests.codes.all_ok)
    assert_that(get_response_data).is_not_empty()
    assert_that(get_response_data).extracting('fname').contains(people_data[0]['fname'], people_data[1]['fname'])
    assert_that(get_response_data).extracting('lname').contains(people_data[0]['lname'], people_data[1]['lname'])
    for i in range(len(get_response_data)):
        assert_that(get_response_data[i]).contains_key('person_id', 'timestamp')


def test_get_person_with_invalid_id_but_valid_payload_returns_404(people):
    people.person_id = 20000000000000

    get_response = people.get_person_by_id(people.person_id)
    get_response.data = get_response.json()

    assert_that(get_response.status_code).is_equal_to(requests.codes.not_found)
    assert_that(get_response.data['title']).is_equal_to('Not Found')
    assert_that(get_response.data['detail']).is_equal_to(f'Person with person_id {people.person_id} not found')

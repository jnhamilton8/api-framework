import pytest
import requests
from assertpy import assert_that

from core.people import People
from tests.people_utils import PeopleUtils


@pytest.fixture(autouse=True)
def people():
    people = People()
    people.first_name = PeopleUtils.random_string_generator(5)
    people.last_name = PeopleUtils.random_string_generator(8)
    people.person_id = PeopleUtils.post_new_person_and_return_person_id(people, people.first_name,
                                                                        people.last_name)
    yield people

    # teardown
    people.delete(people.person_id)


def test_update_person_by_person_id(people):
    amended_first_name = PeopleUtils.random_string_generator(6)
    amended_last_name = PeopleUtils.random_string_generator(10)
    payload = PeopleUtils.create_new_person(amended_first_name, amended_last_name)

    put_response = people.put_person_by_id(people.person_id, payload)
    put_response_data = put_response.json()

    assert_that(put_response.status_code, description='Get failed to return response').is_equal_to(
        requests.codes.all_ok)
    assert_that(put_response_data).contains_entry({'fname': amended_first_name}, {'lname': amended_last_name})
    assert_that(put_response_data).contains_key('person_id', 'timestamp')


def test_update_person_first_name_by_person_id(people):
    amended_first_name = PeopleUtils.random_string_generator(6)
    payload = {'fname': amended_first_name}

    put_response = people.put_person_by_id(people.person_id, payload)
    put_response_data = put_response.json()

    assert_that(put_response.status_code, description='Get failed to return response').is_equal_to(
        requests.codes.all_ok)
    assert_that(put_response_data).contains_entry({'fname': amended_first_name})
    assert_that(put_response_data).contains_key('person_id', 'timestamp')


def test_update_person_last_name_by_person_id(people):
    amended_last_name = PeopleUtils.random_string_generator(6)
    payload = {'lname': amended_last_name}

    put_response = people.put_person_by_id(people.person_id, payload)
    put_response_data = put_response.json()

    assert_that(put_response.status_code, description='Get failed to return response').is_equal_to(
        requests.codes.all_ok)
    assert_that(put_response_data).contains_entry({'lname': amended_last_name})
    assert_that(put_response_data).contains_key('person_id', 'timestamp')


def test_update_person_with_empty_body_returns_unaltered_record(people):
    payload = {}

    put_response = people.put_person_by_id(people.person_id, payload)
    put_response_data = put_response.json()

    assert_that(put_response.status_code, description='Get failed to return response').is_equal_to(
        requests.codes.all_ok)
    assert_that(put_response_data).contains_entry({'fname': people.first_name}, {'lname': people.last_name})
    assert_that(put_response_data).contains_key('person_id', 'timestamp')


def test_updating_person_with_invalid_id_but_valid_payload_returns_404(people):
    people.person_id = 20000000000000
    amended_first_name = PeopleUtils.random_string_generator(6)
    amended_last_name = PeopleUtils.random_string_generator(10)
    payload = PeopleUtils.create_new_person(amended_first_name, amended_last_name)

    put_response = people.put_person_by_id(people.person_id, payload)
    put_response.data = put_response.json()

    assert_that(put_response.status_code).is_equal_to(requests.codes.not_found)
    assert_that(put_response.data['title']).is_equal_to('Not Found')
    assert_that(put_response.data['detail']).is_equal_to(f'Person with id: {people.person_id} not found')


def test_updating_person_omitting_id_returns_404(people):
    people.person_id = None
    amended_first_name = PeopleUtils.random_string_generator(6)
    amended_last_name = PeopleUtils.random_string_generator(10)
    payload = PeopleUtils.create_new_person(amended_first_name, amended_last_name)

    put_response = people.put_person_by_id(people.person_id, payload)
    put_response_data = put_response.json()

    assert_that(put_response.status_code).is_equal_to(requests.codes.not_found)
    assert_that(put_response_data['title']).is_equal_to('Not Found')
    assert_that(put_response_data['detail']).is_equal_to('The requested URL was not found on the server. '
                                                         'If you entered the URL manually please check your '
                                                         'spelling and try again.')

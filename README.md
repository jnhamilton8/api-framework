## Setup

Assumes a Windows environment, instructions may differ for other OS's. 

- Ensure pip3 is installed by running `pip3 -V`.  Pip3 should have been installed automatically with python3.
- Install [pipenv for windows](https://www.pythontutorial.net/python-basics/install-pipenv-windows/)

```zsh
# Run the following to instruct pipenv to use python 3.9 (substitute your version of python)
pipenv --python 3.9 
# Install all dependencies in your virtualenv
pipenv install
# set active environment
pipenv shell
```

## People Repo

The test suite uses the `people-api` 
This project will need to be cloned and set up.  The Github repo is
[here](https://github.com/automationhacks/people-api).

Note that the tests do not rely on any data being present in the database, data is created (and torn down)
as needed per test. 

However, the `people-api` server has to be up and running. Run `python server.py` to start it.

## Pycharm

If using Pycharm, be sure to set the interpreter to use the one created by pipenv e.g. .virtualenvs\api-framework, 
and set the default test runner to be `pytest`

## How to run

```zsh
# Run all tests
python -m pytest

# Run all tests in a module
python pytest test_post_new_person.py 

# Run tests by keywork expressions
python pytest -k "insert desired expression"

# Run tests in parallel
python -m pytest -n auto
```

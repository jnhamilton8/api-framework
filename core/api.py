import logging

import requests

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# urllib3 logger
urllib3_log = logging.getLogger('urllib3')
urllib3_log.setLevel(logging.DEBUG)

# logging to file
fh = logging.FileHandler("api_framework.log")
logger.addHandler(fh)
urllib3_log.addHandler(fh)

PEOPLE_URI = 'http://localhost:5000/api/people'

headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
}


def _format_url(param):
    return PEOPLE_URI + f'/{param}'


class Api(object):
    def __init__(self, timeout=5.0):
        self.timeout = timeout

    def post(self, data=None, json=None):
        logger.info(f'Calling API Method: POST | URL: {PEOPLE_URI}')
        return self._log_and_return(
            requests.post(PEOPLE_URI, data, json, headers=headers, timeout=self.timeout))

    def delete(self, people_id):
        url = _format_url(people_id)
        logger.info(f'Calling API Method: DELETE | URL: {url}')
        return self._log_and_return(requests.delete(url, headers=headers, timeout=self.timeout))

    def get(self, people_id=None):
        url = _format_url(people_id) if people_id else PEOPLE_URI
        logger.info(f'Calling API Method: GET | URL: {url}')
        return self._log_and_return(requests.get(url, headers=headers, timeout=self.timeout))

    def put(self, people_id, data=None):
        url = _format_url(people_id)
        logger.info(f'Calling API Method: PUT | URL: {url}')
        return self._log_and_return(requests.put(url, data, headers=headers, timeout=self.timeout))

    @staticmethod
    def _log_and_return(request_response: requests.Response):
        logger.info(f'Request URL: {request_response.url}')
        logger.info(f'Response headers: {request_response.headers}')
        logger.info(f'Response status code: {request_response.status_code}')
        logger.info(f'Response text: {request_response.text}')
        return request_response

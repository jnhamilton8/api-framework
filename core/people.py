import json

from core.api import Api


class People(Api):
    def __init__(self, timeout=5.0):
        super().__init__(timeout)
        self.person_ids = []
        self.person_id = None
        self.first_name = None
        self.last_name = None

    def post_new_person(self, data):
        return self.post(json=data)

    def delete_person_by_person_id(self, person_id):
        return self.delete(person_id)

    def get_all_people(self):
        return self.get()

    def get_person_by_id(self, person_id):
        return self.get(person_id)

    def put_person_by_id(self, person_id, data):
        return self.put(person_id, json.dumps(data))
